/*
https://developer.gnome.org/gtk-tutorial/2.90/c39.html
gcc g.c `pkg-config --cflags --libs gtk+-2.0`
./a.out


*/
#include<gtk/gtk.h>

int main(int argc,char *argv[])

{

    GtkWidget *window;

    gtk_init(&argc,&argv);

    window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_widget_show(window);

    gtk_main();

    return 0;
}
