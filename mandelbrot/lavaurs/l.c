/*



gcc l.c -Wall
./a.out


42/56 = 3/4 gcd=14
42/54 =7/9  gcd = 6



*/




#include <stdio.h>


/*

http://classes.yale.edu/fractals/Mandelset/MandelCombinatorics/LavaursAlgorithm/LA.html
Lavaurs' algorithm:
(1)	Connect 1/3 and 2/3,
(2)	Assuming all the numbers of period < k have been connected, connect those of period k, starting with the smallest number not yet connected, 
and connecting it to the next smallest number not yet connected, always making the choices so no connecting arc crosses any other arc
*/


// Note that the code passes around struct values instead of pointers to keep it simple, a practice normally avoided for efficiency reasons.
// http://bytes.com/topic/c/answers/554722-c-code-rational-numbers
// dtimes6
struct rational {
        unsigned int numerator;
        unsigned int denominator;
};
 
struct rational sum( struct rational a, struct rational b) {
    struct rational c;
    c.numerator = a.numerator * b.denominator + b.numerator * a.denominator;
    c.denominator = a.denominator * b.denominator;
    return c;
}
 
struct rational multi( struct rational a, struct rational b) {
    struct rational c;
    c.numerator = a.numerator * b.numerator;
    c.denominator = a.denominator * b.denominator;
    return c;
}

/* Standard C Function: Greatest Common Divisor */
int gcd ( int a, int b )
{
  int c;
  while ( a != 0 ) {
     c = a; 
     a = b%a;  
     b = c;
  }
  return b;
}




int main() {
     

    struct rational r, rr;
    unsigned int g;

    r.numerator = 42;
    r.denominator = 54;
    g = gcd(r.numerator, r.denominator);

    printf("r =  %u / %u ; gcd = %d \n", r.numerator, r.denominator,g);
    if (r.numerator <  r.denominator) 
       printf(" fraction r is proper fraction : r < 1.0  \n" );
       else  printf(" fraction r is unproper fraction : r > 1.0  \n" );
   if (g==1) printf(" fraction r is in lowest terms = irreducible  \n" );
            else {
                   printf(" fraction r is not in lowest terms = reducible \n" ); 
                   rr.numerator= r.numerator/g;
                   rr.denominator = r.denominator/g;
                   printf("reduced r =  %u / %u \n", rr.numerator, rr.denominator);
                  }

   return 0;
}
 


