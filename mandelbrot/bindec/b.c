 /* 
 c program:
  1. draws Mandelbrot set for Fc(z)=z*z +c
   using Madelbrot algorithm ( boolean escape time )
 -------------------------------         
 2. technic of creating ppm file is  based on the code of Claudio Rocchini
 http://en.wikipedia.org/wiki/Image:Color_complex_plot.jpg
 create 24 bit color graphic file ,  portable pixmap file = PPM 
 see http://en.wikipedia.org/wiki/Portable_pixmap
 to see the file use external application ( graphic viewer)
 ---------------------------------
gcc b.c -lm -Wall -Wextra -march=native

a@zalman:~/c/mandelbrot/bindec$ gcc b.c -lm -Wall
a@zalman:~/c/mandelbrot/bindec$ time ./a.out
iYmax= 10000 
iY= 9999 
real	280m7.232s
user	279m57.549s
sys	0m1.411s


 */
 #include <stdio.h>
 #include <math.h>
 int main()
 {
          /* screen ( integer) coordinate */
        int iX,iY;
        const int iXmax = 10000; 
        const int iYmax = 10000;
        /* world ( double) coordinate = parameter plane*/
        double Cx,Cy;     
        const double CxMin=-2.5;
        const double CxMax=1.5;
        const double CyMin=-2.0;
        const double CyMax=2.0;
        /* */
        double PixelWidth=(CxMax-CxMin)/iXmax;
        double PixelHeight=(CyMax-CyMin)/iYmax;
         /* color component ( R or G or B) is coded from 0 to 255 */
        /* it is 24 bit color RGB file */
        const int MaxColorComponentValue=255; 
        FILE * fp;
        char *filename="new1.ppm";
        char *comment="# binary decomposition of level sets of exterior of Mandelbrot set by Adam Majewski";/* comment should start with # */
        static unsigned char color[3];
        /* Z=Zx+Zy*i  ;   Z0 = 0 */
        double Zx, Zy;
        double Zx2, Zy2; /* Zx2=Zx*Zx;  Zy2=Zy*Zy  */
        /*  */
        int Iteration;
        const int IterationMax=200000;
        /* bail-out value , radius of circle ;  */
        const double EscapeRadius=10;
        double ER2=EscapeRadius*EscapeRadius;



        printf( "iYmax= %d \n; ", iYmax);
        /*create new file,give it a name and open it in binary mode  */
        fp= fopen(filename,"wb"); /* b -  binary mode */
        /*write ASCII header to the file*/
        fprintf(fp,"P6\n %s\n %d\n %d\n %d\n",comment,iXmax,iYmax,MaxColorComponentValue);
        /* compute and write image data bytes to the file*/
        for(iY=0;iY<iYmax;iY++)
        {
             printf( "iY= %d \r", iY); // info about progress 
             Cy=CyMin + iY*PixelHeight;
             if (fabs(Cy)< PixelHeight/2) Cy=0.0; /* Main antenna */
             
             for(iX=0;iX<iXmax;iX++)
             {         
                       Cx=CxMin + iX*PixelWidth;
                        /* initial value of orbit = critical point Z= 0 */
                        Zx=0.0;
                        Zy=0.0;
                        Zx2=Zx*Zx;
                        Zy2=Zy*Zy;
                        /* */
                        for (Iteration=0;Iteration<IterationMax && ((Zx2+Zy2)<ER2);Iteration++)
                        {
                            Zy=2*Zx*Zy + Cy;
                            Zx=Zx2-Zy2 +Cx;
                            Zx2=Zx*Zx;
                            Zy2=Zy*Zy;
                        };
                        /* compute  pixel color (24 bit = 3 bajts) */
                        if (Iteration==IterationMax)
                        { /*  interior of Mandelbrot set = black */
                           color[0]=0;
                           color[1]=0;
                           color[2]=0;                           
                        }
                  /* exterior of Mandelbrot set = binary decomposition of level sets of escape time */
                        else if (Zy>0) 
                           { 
                             color[0]=0; /* Red*/
                             color[1]=0;  /* Green */ 
                             color[2]=0;/* Blue */
                           }
                           else 
                           {
                             color[0]=255; /* Red*/
                             color[1]=255;  /* Green */ 
                             color[2]=255;/* Blue */    
                           };
                       /*write color to the file*/
                        fwrite(color,1,3,fp);
                }
        }
        fclose(fp);
       return 0;
 }
