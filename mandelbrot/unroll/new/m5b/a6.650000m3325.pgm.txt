IterationMax = 33250 
EscapeRadius = 2.000000 
CDistanceMax = 0.000007 

K plane : 
dKSide = 0.333300 

Corners of rectangle on K-plane : 
  KxMin = 2.246445 
 KxMax = 4.492890 
 KyMin = 0.000000 
 KyMax = 0.333300 

corners of strip on C-plane: 
  CxMin = 0.360680 
 CxMax = 0.292745 
 CyMin = 0.402632 
 CyMax = 0.296383 

