IterationMax = 66500 
EscapeRadius = 30.000000 
CDistanceMax = 0.000002108119974 

K plane : 
dKSide = 0.333300 

Corners of rectangle on K-plane : 
  KxMin = 4.492890 
 KxMax = 6.739335 
 KyMin = 0.000000 
 KyMax = 0.333300 

corners of strip on C-plane: 
  CxMin = 0.292745 
 CxMax = 0.270615 
 CyMin = 0.296383 
 CyMax = 0.271381 

