IterationMax = 66500 
EscapeRadius = 2.000000 
CDistanceMax = 0.000025 

K plane : 
dKSide = 0.333300 

Corners of rectangle on K-plane : 
  KxMin = 0.000000 
 KxMax = 2.166445 
 KyMin = 0.000000 
 KyMax = 0.333300 

corners of strip on C-plane: 
  CxMin = -0.750000 
 CxMax = 0.363941 
 CyMin = -1.999775 
 CyMax = 0.410954 

