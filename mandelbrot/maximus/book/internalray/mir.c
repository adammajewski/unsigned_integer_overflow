/*

mandelbrot_internal_ray.c
mir.c
gcc mir.c -std=c99 -Wall -Wextra -pedantic -lm -lmpfr -lgmp 


./bin/mandelbrot_internal_ray 53 -1 0 2 0 100 1000
 
 shows that tracing internal rays towards the root (angle = 0) works ok 
 as long as radius < 1, modifying the code (step + 1.0) shows that when 
 radius = 1 the Newton's method converges to the parent's root 0.25 instead.


 program uses the code by Claude Heiland-Allen
 from https://gitorious.org/maximus/book/




*/


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <complex.h>
#include <gmp.h>
#include <mpfr.h>



const double pi = 3.14159265358979323846264338327950288419716939937510;




void usage(const char *progname) {
  fprintf(stderr,
    "usage: %s bits nucleusx nucleusy period angle maxiters steps\n"
    "outputs space separated complex numbers on stdout\n"
    "example %s 53 0 0 1 1/3 100 1000\n"
    "or  %s 53 0 0 1 1/3 100 1000 > iray.txt\n",
    progname, progname, progname);
}


/*
 > Interior is described in book/interior coordinate ?

not really - that's for finding b given c, mandelbrot_interior() finds c 
given b (which is much harder).

 but there is a stub with the algorithm description:
3.3 Newton’s Method: Bond

http://mathr.co.uk/blog/2013-04-01_interior_coordinates_in_the_mandelbrot_set.html
*/
extern int mandelbrot_interior(mpfr_t wucleusx, mpfr_t wucleusy, mpfr_t bondx, mpfr_t bondy, const mpfr_t wucleusguessx, const mpfr_t wucleusguessy, const mpfr_t bondguessx, const mpfr_t bondguessy, int period, const mpfr_t interiorx, const mpfr_t interiory, int maxiters) {
  int retval = 0;
#define VARS bzx, bzy, bcx, bcy, ax, ay, bx, by, cx, cy, dx, dy, ex, ey, s, t, u, v, detx, dety, det
  mpfr_t VARS;
  mpfr_inits2(mpfr_get_prec(bondguessx), VARS, (mpfr_ptr) 0);
  mpfr_set(bzx, wucleusguessx, GMP_RNDN);
  mpfr_set(bzy, wucleusguessy, GMP_RNDN);
  mpfr_set(bcx, bondguessx, GMP_RNDN);
  mpfr_set(bcy, bondguessy, GMP_RNDN);
  for (int i = 0; i < maxiters; ++i) {
    // a = bz
    mpfr_set(ax, bzx, GMP_RNDN);
    mpfr_set(ay, bzy, GMP_RNDN);
    // b = 1
    mpfr_set_si(bx, 1, GMP_RNDN);
    mpfr_set_si(by, 0, GMP_RNDN);
    // c = 0
    mpfr_set_si(cx, 0, GMP_RNDN);
    mpfr_set_si(cy, 0, GMP_RNDN);
    // d = 0
    mpfr_set_si(dx, 0, GMP_RNDN);
    mpfr_set_si(dy, 0, GMP_RNDN);
    // e = 0
    mpfr_set_si(ex, 0, GMP_RNDN);
    mpfr_set_si(ey, 0, GMP_RNDN);
    for (int p = 0; p < period; ++p) {
      // e = 2 * (a * e + b * d);
      mpfr_mul(s, ax, ex, GMP_RNDN);
      mpfr_mul(t, ay, ey, GMP_RNDN);
      mpfr_mul(u, ax, ey, GMP_RNDN);
      mpfr_mul(v, ay, ex, GMP_RNDN);
      mpfr_sub(ex, s, t, GMP_RNDN);
      mpfr_add(ey, u, v, GMP_RNDN);
      mpfr_mul(s, bx, dx, GMP_RNDN);
      mpfr_mul(t, by, dy, GMP_RNDN);
      mpfr_mul(u, bx, dy, GMP_RNDN);
      mpfr_mul(v, by, dx, GMP_RNDN);
      mpfr_sub(s, s, t, GMP_RNDN);
      mpfr_add(u, u, v, GMP_RNDN);
      mpfr_add(ex, ex, s, GMP_RNDN);
      mpfr_add(ey, ey, u, GMP_RNDN);
      mpfr_mul_2si(ex, ex, 1, GMP_RNDN);
      mpfr_mul_2si(ey, ey, 1, GMP_RNDN);
      // d = 2 * a * d + 1;
      mpfr_mul(s, ax, dx, GMP_RNDN);
      mpfr_mul(t, ay, dy, GMP_RNDN);
      mpfr_mul(u, ax, dy, GMP_RNDN);
      mpfr_mul(v, ay, dx, GMP_RNDN);
      mpfr_sub(dx, s, t, GMP_RNDN);
      mpfr_add(dy, u, v, GMP_RNDN);
      mpfr_mul_2si(dx, dx, 1, GMP_RNDN);
      mpfr_mul_2si(dy, dy, 1, GMP_RNDN);
      mpfr_add_si(dx, dx, 1, GMP_RNDN);
      // c = 2 * (b^2 + a * c);
      mpfr_mul(s, ax, cx, GMP_RNDN);
      mpfr_mul(t, ay, cy, GMP_RNDN);
      mpfr_mul(u, ax, cy, GMP_RNDN);
      mpfr_mul(v, ay, cx, GMP_RNDN);
      mpfr_sub(s, s, t, GMP_RNDN);
      mpfr_add(t, u, v, GMP_RNDN);
      mpfr_sqr(u, bx, GMP_RNDN);
      mpfr_sqr(v, by, GMP_RNDN);
      mpfr_sub(u, u, v, GMP_RNDN);
      mpfr_mul(v, bx, by, GMP_RNDN);
      mpfr_mul_2si(v, v, 1, GMP_RNDN);
      mpfr_add(cx, s, u, GMP_RNDN);
      mpfr_add(cy, t, v, GMP_RNDN);
      mpfr_mul_2si(cx, cx, 1, GMP_RNDN);
      mpfr_mul_2si(cy, cy, 1, GMP_RNDN);
      // b = 2 * a * b;
      mpfr_mul(s, ax, bx, GMP_RNDN);
      mpfr_mul(t, ay, by, GMP_RNDN);
      mpfr_mul(u, ax, by, GMP_RNDN);
      mpfr_mul(v, ay, bx, GMP_RNDN);
      mpfr_sub(bx, s, t, GMP_RNDN);
      mpfr_add(by, u, v, GMP_RNDN);
      mpfr_mul_2si(bx, bx, 1, GMP_RNDN);
      mpfr_mul_2si(by, by, 1, GMP_RNDN);
      // a = a^2 + bc;
      mpfr_sqr(u, ax, GMP_RNDN);
      mpfr_sqr(v, ay, GMP_RNDN);
      mpfr_sub(u, u, v, GMP_RNDN);
      mpfr_mul(v, ax, ay, GMP_RNDN);
      mpfr_mul_2si(v, v, 1, GMP_RNDN);
      mpfr_add(ax, u, bcx, GMP_RNDN);
      mpfr_add(ay, v, bcy, GMP_RNDN);
    }
    // det = (b - 1) * e - c * d
    mpfr_sub_si(u,  bx, 1, GMP_RNDN);
    mpfr_mul(s, u, ex, GMP_RNDN);
    mpfr_mul(t, by, ey, GMP_RNDN);
    mpfr_mul(u, u, ey, GMP_RNDN);
    mpfr_mul(v, by, ex, GMP_RNDN);
    mpfr_sub(detx, s, t, GMP_RNDN);
    mpfr_add(dety, u, v, GMP_RNDN);
    mpfr_mul(s, cx, dx, GMP_RNDN);
    mpfr_mul(t, cy, dy, GMP_RNDN);
    mpfr_mul(u, cx, dy, GMP_RNDN);
    mpfr_mul(v, cy, dx, GMP_RNDN);
    mpfr_sub(s, s, t, GMP_RNDN);
    mpfr_add(u, u, v, GMP_RNDN);
    mpfr_sub(detx, detx, s, GMP_RNDN);
    mpfr_sub(dety, dety, u, GMP_RNDN);
    // bz' = bz - (e * (a - bz) - d * (b - interior)) / det
    mpfr_sub(ax, ax, bzx, GMP_RNDN);
    mpfr_sub(ay, ay, bzy, GMP_RNDN);
    mpfr_mul(s, ax, ex, GMP_RNDN);
    mpfr_mul(t, ay, ey, GMP_RNDN);
    mpfr_mul(u, ax, ey, GMP_RNDN);
    mpfr_mul(v, ay, ex, GMP_RNDN);
    mpfr_sub(ex, s, t, GMP_RNDN);
    mpfr_add(ey, u, v, GMP_RNDN);
    mpfr_sub(u, bx, interiorx, GMP_RNDN);
    mpfr_sub(v, by, interiory, GMP_RNDN);
    mpfr_mul(s, u, dx, GMP_RNDN);
    mpfr_mul(t, v, dy, GMP_RNDN);
    mpfr_mul(u, u, dy, GMP_RNDN);
    mpfr_mul(v, v, dx, GMP_RNDN);
    mpfr_sub(s, s, t, GMP_RNDN);
    mpfr_add(u, u, v, GMP_RNDN);
    mpfr_sub(ex, ex, s, GMP_RNDN);
    mpfr_sub(ey, ey, u, GMP_RNDN);
    mpfr_mul(s, detx, ex, GMP_RNDN);
    mpfr_mul(t, dety, ey, GMP_RNDN);
    mpfr_mul(u, detx, ey, GMP_RNDN);
    mpfr_mul(v, dety, ex, GMP_RNDN);
    mpfr_add(ex, s, t, GMP_RNDN);
    mpfr_sub(ey, u, v, GMP_RNDN);
    mpfr_sqr(u, detx, GMP_RNDN);
    mpfr_sqr(v, dety, GMP_RNDN);
    mpfr_add(det, u, v, GMP_RNDN);
    mpfr_div(ex, ex, det, GMP_RNDN);
    mpfr_div(ey, ey, det, GMP_RNDN);
    mpfr_sub(bzx, bzx, ex, GMP_RNDN);
    mpfr_sub(bzy, bzy, ey, GMP_RNDN);
    mpfr_set(wucleusx, bzx, GMP_RNDN);
    mpfr_set(wucleusy, bzy, GMP_RNDN);
    // bc' = bc - ((b - 1)(b - interior) - c * (a - bz)) / det
    mpfr_mul(s, ax, cx, GMP_RNDN);
    mpfr_mul(t, ay, cy, GMP_RNDN);
    mpfr_mul(u, ax, cy, GMP_RNDN);
    mpfr_mul(v, ay, cx, GMP_RNDN);
    mpfr_sub(cx, s, t, GMP_RNDN);
    mpfr_add(cy, u, v, GMP_RNDN);
    mpfr_sub_si(u, bx, 1, GMP_RNDN);
    mpfr_set(v, by, GMP_RNDN);
    mpfr_sub(s, bx, interiorx, GMP_RNDN);
    mpfr_sub(t, by, interiory, GMP_RNDN);
    mpfr_mul(ex, u, s, GMP_RNDN);
    mpfr_mul(ey, u, t, GMP_RNDN);
    mpfr_mul(dx, v, s, GMP_RNDN);
    mpfr_mul(dy, v, t, GMP_RNDN);
    mpfr_sub(bx, ex, dy, GMP_RNDN);
    mpfr_add(by, ey, dx, GMP_RNDN);
    mpfr_sub(bx, bx, cx, GMP_RNDN);
    mpfr_sub(by, by, cy, GMP_RNDN);
    mpfr_mul(s, detx, bx, GMP_RNDN);
    mpfr_mul(t, dety, by, GMP_RNDN);
    mpfr_mul(u, detx, by, GMP_RNDN);
    mpfr_mul(v, dety, bx, GMP_RNDN);    
    mpfr_add(bx, s, t, GMP_RNDN);
    mpfr_sub(by, u, v, GMP_RNDN);
    mpfr_div(bx, bx, det, GMP_RNDN);
    mpfr_div(by, by, det, GMP_RNDN);
    mpfr_sub(bondx, bcx, bx, GMP_RNDN);
    mpfr_sub(bondy, bcy, by, GMP_RNDN);
    // check if delta is small
    mpfr_sub(u, bondx, bcx, GMP_RNDN);
    mpfr_sub(v, bondy, bcy, GMP_RNDN);
    if (mpfr_zero_p(u) && mpfr_zero_p(v)) {
      retval = 1;
      goto done;
    }
    mpfr_set(bcx, bondx, GMP_RNDN);
    mpfr_set(bcy, bondy, GMP_RNDN);
  }
done:
  mpfr_clears(VARS, (mpfr_ptr) 0);
#undef VARS
  return retval;
}











int main(int argc, char **argv) {
  if (argc != 8) { usage(argv[0]); return 1; }
  int bits = atoi(argv[1]);
  mpfr_t nucleusx, nucleusy, boundaryx, boundaryy, wucleusx, wucleusy, interiorx, interiory;
  mpfr_inits2(bits, nucleusx, nucleusy, boundaryx, boundaryy, wucleusx, wucleusy, (mpfr_ptr) 0);
  mpfr_inits2(53, interiorx, interiory, (mpfr_ptr) 0);
  mpfr_set_str(nucleusx, argv[2], 10, GMP_RNDN);
  mpfr_set_str(nucleusy, argv[3], 10, GMP_RNDN);
  mpfr_set(wucleusx, nucleusx, GMP_RNDN);
  mpfr_set(wucleusy, nucleusy, GMP_RNDN);
  mpfr_set(boundaryx, nucleusx, GMP_RNDN);
  mpfr_set(boundaryy, nucleusy, GMP_RNDN);
  int period = atoi(argv[4]);
  mpq_t q;
  mpq_init(q);
  mpq_set_str(q, argv[5], 0);
  mpq_canonicalize(q);
  double angle = 2.0 * pi * mpq_get_d(q);
  complex double phase = cexp(I * angle);
  int maxiters = atoi(argv[6]);
  int steps = atoi(argv[7]);


  for (int step = 0; step < steps; ++step) {
    double radius = (step + 0.5) / steps;
    complex double interior = radius * phase;
    mpfr_set_d(interiorx, creal(interior), GMP_RNDN);
    mpfr_set_d(interiory, cimag(interior), GMP_RNDN);
    mandelbrot_interior(wucleusx, wucleusy, boundaryx, boundaryy, wucleusx, wucleusy, boundaryx, boundaryy, period, interiorx, interiory, maxiters);
    mpfr_out_str(0, 10, 0, boundaryx, GMP_RNDN);
    putchar(' ');
    mpfr_out_str(0, 10, 0, boundaryy, GMP_RNDN);
    putchar('\n');
  }
  mpq_clear(q);
  mpfr_clears(wucleusx, wucleusy, boundaryx, boundaryy, nucleusx, nucleusy, interiorx, interiory, (mpfr_ptr) 0);
  return 0;
}
