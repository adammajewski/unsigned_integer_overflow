#include <stdio.h>
#include <stdlib.h>
#include "mandelbrot_binary_angle.h"
#include "mandelbrot_external_ray_in.h"

void usage(const char *progname) {
  fprintf(stderr,
    "usage: %s angle depth\n"
    "outputs space-separated complex numbers on stdout\n"
    "example %s 1/3 25\n",
    progname, progname);
}

int main(int argc, char **argv) {

  if (argc < 3) { usage(argv[0]); return 1; }



  mpq_t angle;
  mpq_init(angle);
  if (argv[1][0] == '.') {
    struct mandelbrot_binary_angle *a = mandelbrot_binary_angle_from_string(argv[1]);
    if (a) {
      mandelbrot_binary_angle_to_rational(angle, a);
      mandelbrot_binary_angle_delete(a);
    } else {
      usage(argv[0]);
      return 1;
    }
  } else {
    mpq_set_str(angle, argv[1], 0);
  }
  mpq_canonicalize(angle);



  int depth = atoi(argv[2]);
  struct mandelbrot_external_ray_in *ray = mandelbrot_external_ray_in_new(angle);
  mpfr_t cre, cim;
  mpfr_init2(cre, 53);
  mpfr_init2(cim, 53);
  for (int i = 0; i < depth * 4; ++i) { // FIXME 4 = implementation's sharpness
    mandelbrot_external_ray_in_step(ray);
    mandelbrot_external_ray_in_get(ray, cre, cim);
    mpfr_out_str(0, 10, 0, cre, GMP_RNDN);
    putchar(' ');
    mpfr_out_str(0, 10, 0, cim, GMP_RNDN);
    putchar('\n');
  }
  mpfr_clear(cre);
  mpfr_clear(cim);
  mandelbrot_external_ray_in_delete(ray);
  mpq_clear(angle);
  return 0;
}
