/*


 gcc -std=c99 -Wall -pedantic -Wextra -O3 e.c -lgmp -lm

see attached C code using double precision, also gmp for rationals.  I 
tried to stick to the structure of your maxima code (though I prefer the 
struct stepper pattern I wrote in my own library code as it is more 
flexible/reusable than printing in a loop).  I fixed some bugs (m should 
depend on s, but not d, and I offset it by 0.5 to avoid possible edge 
conditions, also angle needs doubling mod 1 each time d is increased).

http://mathr.co.uk/blog/
https://gitorious.org/~claude


Claude Heiland-Allen

 ./a.out


*/





#include <complex.h>
#include <math.h>
#include <stdio.h>
#include <gmp.h>

const double pi = 3.141592653589793;

complex double GiveNewC(complex double t, complex double oldC, int depth) {
  complex double cPrev = oldC, cNext = oldC;
  for (int newtonStep = 0; newtonStep < 64; ++newtonStep) {
    complex double z = 0;
    complex double d = 0;

     /*  nested loop for the z^2+c etc that runs (depth) times  */
    for (int i = 0; i <depth; ++i) {
      d = 2 * z * d + 1;
      z = z * z + cPrev;
    }
    cNext = cPrev - (z - t) / d;
    if (! (cabs(cNext - cPrev) > 1e-12)) { break; }
    cPrev = cNext;
  }
  return cNext;
}



void GiveRay(mpq_t angle, int depth, int sharpness) {
  // one = 1/1
  mpq_t one;
  mpq_init(one);
  mpq_set_si(one, 1, 1);
  double R = 65536;

  complex double oldC = R * cexp(2 * pi * I * mpq_get_d(angle)), newC;

  for (int d = 1; d <= depth; ++d) {

    for (int s = 1; s <= sharpness; ++s) {
      double m = s - 0.5;
      double r = pow(R, pow(0.5, m / sharpness));
      complex double t = r * cexp(2 * pi * I * mpq_get_d(angle));
      newC = GiveNewC(t, oldC, d);
      printf("d = %d ; s = %d ; c = %.14f + %.14f i\n", d, s, creal(newC), cimag(newC));
      oldC = newC;
    }
    // angle = 2 * angle (mod 1)
    mpq_mul_2exp(angle, angle, 1);
    if (mpq_cmp_ui(angle, 1, 1) >= 0) {
      mpq_sub(angle, angle, one);
    }
  }
  mpq_clear(one);
}




int main(int argc, char **argv) {
  (void) argc;
  (void) argv;
  mpq_t angle;
  mpq_init(angle);
  mpq_set_si(angle, 1, 7);
  GiveRay(angle, 4000, 1);
  return 0;
}
