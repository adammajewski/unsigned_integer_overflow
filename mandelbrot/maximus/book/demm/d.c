struct mandelbrot_pixel_exterior {
  int final_n;
  int final_p;
  complex float final_z;
  float final_de;
};

struct mandelbrot_pixel_interior {
  int final_p;
  complex float final_dz;
  float final_de;
};

enum mandelbrot_pixel_type {
  mandelbrot_pixel_interior = -1,
  mandelbrot_pixel_unescaped = 0,
  mandelbrot_pixel_exterior = 1
};

struct mandelbrot_pixel {
  enum mandelbrot_pixel_type tag;
  union {
    struct mandelbrot_pixel_exterior exterior;
    struct mandelbrot_pixel_interior interior;
  } u;
};

struct mandelbrot_image {
  int width;
  int height;
  struct mandelbrot_pixel *pixels;


static void calculate_exterior_mpfr(struct mandelbrot_pixel *out, const mpfr_t cre, const mpfr_t cim, int maximum_iterations, const mpfr_t escape_radius2, const mpfr_t pixel_spacing) {
  out->tag = mandelbrot_pixel_unescaped;
#define VARS zre, zim, dcre, dcim, mz2, z2, t0, t1, t2, t3, t4, t5
  mpfr_t VARS;
  mpfr_inits2(mpfr_get_prec(cre), VARS, (mpfr_ptr) 0);
  // z := 0
  cmpfr_set_si(zre, zim, 0, 0, GMP_RNDN);
  // dc := 0
  cmpfr_set_si(dcre, dcim, 0, 0, GMP_RNDN);
  // mz2 := 1.0/0.0
  mpfr_set_d(mz2, 1.0 / 0.0, GMP_RNDN);
  int p = 0;
  for (int n = 1; n < maximum_iterations; ++n) {
    // dc := 2 * z * dc + 1
    cmpfr_mul(dcre, dcim, zre, zim, dcre, dcim, t0, t1, t2, t3, GMP_RNDN);
    cmpfr_mul_2si(dcre, dcim, dcre, dcim, 1, GMP_RNDN);
    mpfr_add_si(dcre, dcre, 1, GMP_RNDN);
    // z := z^2 + c
    cmpfr_sqr(zre, zim, zre, zim, t0, t1, GMP_RNDN);
    cmpfr_add(zre, zim, zre, zim, cre, cim, GMP_RNDN);
    // z2 := |z|^2
    cmpfr_abs2(z2, zre, zim, t0, t1, GMP_RNDN);
    if (mpfr_less_p(z2, mz2)) {
      // mz2 := z2;
      mpfr_set(mz2, z2, GMP_RNDN);
      p = n;
    }
    if (mpfr_greater_p(z2, escape_radius2)) {
      out->tag = mandelbrot_pixel_exterior;
      out->u.exterior.final_n = n;
      out->u.exterior.final_z = mpfr_get_d(zre, GMP_RNDN) + I * mpfr_get_d(zim, GMP_RNDN);
      // de := 2 * |z| * log(|z|) / (|dc| * pixel_spacing)
      cmpfr_abs2(t4, zre, zim, t0, t1, GMP_RNDN);
      mpfr_sqrt(t4, t4, GMP_RNDN);
      mpfr_log(t5, t4, GMP_RNDN);
      cmpfr_abs2(t3, dcre, dcim, t0, t1, GMP_RNDN);
      mpfr_sqrt(t3, t3, GMP_RNDN);
      mpfr_mul(t3, t3, pixel_spacing, GMP_RNDN);
      mpfr_mul(t0, t4, t5, GMP_RNDN);
      mpfr_mul_2si(t0, t0, 1, GMP_RNDN);
      mpfr_div(t0, t0, t3, GMP_RNDN);
      out->u.exterior.final_de = mpfr_get_d(t0, GMP_RNDN);
      out->u.exterior.final_p = p;
      break;
    }
  }
  mpfr_clears(VARS, (mpfr_ptr) 0);
#undef VARS
}

