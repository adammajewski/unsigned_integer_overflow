/* 
 c program:
  1. draws Fatou set for Fc(z)=z*z 
  using  binary decomposition
 -------------------------------         
 2. technic of creating ppm file is  based on the code of Claudio Rocchini
 http://en.wikipedia.org/wiki/Image:Color_complex_plot.jpg
 create 24 bit color graphic file ,  portable pixmap file = PPM 
 see http://en.wikipedia.org/wiki/Portable_pixmap
 to see the file use external application ( graphic viewer)
 ---------------------------------
 */
 #include <stdio.h>
 #include <stdlib.h> /* for ISO C Random Number Functions */
 #include <math.h>
 
 /* ----------------------*/
 int main()
 {   
    const double Cx=0.285,Cy=0.01;
     /* screen coordinate = coordinate of pixels */      
    int iX, iY, 
        iXmin=0, iXmax=10000,
        iYmin=0, iYmax=10000,
        iWidth=iXmax-iXmin+1,
        iHeight=iYmax-iYmin+1,
        /* 3D data : X , Y, color */
        /* number of bytes = number of pixels of image * number of bytes of color */
        iLength=iWidth*iHeight*3,/* 3 bytes of color  */
        index; /* of array */
   // int iXinc, iYinc,iIncMax=6;     
    /* world ( double) coordinate = parameter plane*/
    const double ZxMin=-1.5;
    const double ZxMax=1.5;
    const double ZyMin=-1.5;
    const double ZyMax=1.5;
    /* */
    double PixelWidth=(ZxMax-ZxMin)/iWidth;
    double PixelHeight=(ZyMax-ZyMin)/iHeight;
    double Zx, Zy,    /* Z=Zx+Zy*i   */
           Z0x, Z0y,  /* Z0 = Z0x + Z0y*i */
           Zx2, Zy2; /* Zx2=Zx*Zx;  Zy2=Zy*Zy  */
           //NewZx, NewZy,
          // DeltaX, DeltaY,
         //  SqrtDeltaX, SqrtDeltaY,
         //  AlphaX, AlphaY,
         //  BetaX,BetaY, /* repelling fixed point Beta */
        //   AbsLambdaA,AbsLambdaB;
          /*  */
        int Iteration,
            IterationMax=4000000,
            iTemp;
      unsigned char iColor;
     /* bail-out value , radius of circle ;  */
    const int EscapeRadius=4000;
    int ER2=EscapeRadius*EscapeRadius;
    //double AR=PixelWidth, /* minimal distance from attractor = Attractor Radius */
           //AR2=AR*AR,
          // d,dX,dY; /*  distance from attractor : d=sqrt(dx*dx+dy*dy) */
        /* PPM file */
    FILE * fp;
    char *filename="bluewhite.ppm";
    char *comment="# this is julia set for fc(z) = z^2+ 0.285+0.01i ";/* comment should start with # */
    const int MaxColorComponentValue=255;/* color component ( R or G or B) is coded from 0 to 255 */
    /* dynamic 1D array for 24-bit color values */    
    unsigned char *array;
   
   printf(" Cx= %f\n",Cx);
   printf(" Cy= %f\n",Cy); 
  
  
   /*-------------------------------------------------------------------*/
    array = malloc( iLength * sizeof(unsigned char) );
    if (array == NULL)
    {
      fprintf(stderr,"Could not allocate memory");
      getchar();
      return 1;
    }
    else 
    {         
      /* fill the data array with white points */       
      for(index=0;index<iLength-1;++index) array[index]=255;
      /* ---------------------------------------------------------------*/
      for(iY=0;iY<iYmax;++iY)
      {
          Z0y=ZyMin + iY*PixelHeight; /* reverse Y  axis */
             if (fabs(Z0y)<PixelHeight/2) Z0y=0.0; /*  */    
         for(iX=0;iX<iXmax;++iX)
         {    /* initial value of orbit Z0 */
             
              Z0x=ZxMin + iX*PixelWidth;
              /* Z = Z0 */
              Zx=Z0x;
              Zy=Z0y;
              Zx2=Zx*Zx;
              Zy2=Zy*Zy;
             /* */
             Iteration=0;
             while (Iteration<IterationMax && ((Zx2+Zy2)<ER2))
                    {
                        Zy=2*Zx*Zy + Cy;
                        Zx=Zx2-Zy2 +Cx;
                        Zx2=Zx*Zx;
                        Zy2=Zy*Zy;
                        Iteration+=1;
                    };
           iTemp=((iYmax-iY-1)*iXmax+iX)*3;        
           iColor = Iteration %255; // !!!!!
           /* compute  pixel color (24 bit = 3 bajts) */
           
                 /* only exterior of disconnected  Julia set  */
              /*  */
                        
                         array[iTemp]=255; /* Red*/
                         array[iTemp+1]=255;  /* Green */ 
                         array[iTemp+2]=iColor;/* Blue */
                                             
   /* check the orientation of Z-plane */
               /* mark first quadrant of cartesian plane*/     
             //  if (Z0x>0 && Z0y>0) array[((iYmax-iY-1)*iXmax+iX)*3]=255-array[((iYmax-iY-1)*iXmax+iX)*3];  
     }
    } 
 
 /* write the whole data array to ppm file in one step ----------------------- */      
      /*create new file,give it a name and open it in binary mode  */
      fp= fopen(filename,"wb"); /* b -  binary mode */
      if (fp == NULL){ fprintf(stderr,"file error"); }
            else
            {
            /*write ASCII header to the file*/
            fprintf(fp,"P6\n %s\n %d\n %d\n %d\n",comment,iXmax,iYmax,MaxColorComponentValue);
            /*write image data bytes to the file*/
            fwrite(array,iLength ,1,fp);
            fclose(fp);
            fprintf(stderr,"file saved");
            //getchar();
            }
    free(array);
    return 0;
    } /* if (array ..  else ... */
 }
