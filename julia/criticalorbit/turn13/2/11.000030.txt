This image shows part of dynamical z-plane of discrete dynamical system  
 map fc is a complex quadratic polynomial : fc(z) = z^2 + c  
 black points show critical orbit = orbit of critical point z = 0   
   
Initial InternalAngle or rotational number = 1/3 + 0.000030 = 0.333363 
   
parameter of map fc :   
Cx  = -0.125163 
Cy  = 0.649519 
is located on iPeriodParent = 1 
iPeriodChild = 3 
   
alfa fixed point  
alfax  = -0.250082 
alfay  = 0.432966 
   
corners of dynamical z-plane rectangle : 
  ZxMin = -0.750082 
 ZxMax = 0.249918 
 ZyMin = -0.192034 
 ZyMax = 0.807966 
iHeight  = 2000 
PixelWidth  = 0.000500 
Number of computed critical orbit points  = iterMax = 100000000000 
distorsion of image  = 1.000000 
Number of computed but clipped critical orbit points ( out of image so not drawn)  = iLostPixels = 0 
