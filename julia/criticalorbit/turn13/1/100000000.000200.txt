This image shows part of dynamical z-plane of discrete dynamical system  
 map fc is a complex quadratic polynomial : fc(z) = z^2 + c  
 black points show critical orbit = orbit of critical point z = 0   
   
iPeriodParent = 1 
iPeriodChild = 3 
InternalAngle or rotational number = 0.333333 
InternalAngle distorsion ( epsilon)  = 0.000200 
   
parameter of map fc :   
Cx  = -0.126088 
Cy  = 0.649518 
   
alfa fixed point  
alfax  = -0.250544 
alfay  = 0.432698 
   
corners of dynamical z-plane rectangle : 
  ZxMin = -0.750544 
 ZxMax = 0.249456 
 ZyMin = -0.192302 
 ZyMax = 0.807698 
iHeight  = 2000 
PixelWidth  = 0.000500 
Number of computed critical orbit points  = iterMax = 100000000 
distorsion of image  = 1.000000 
Number of computed but clipped critical orbit points ( out of image so not drawn)  = iLostPixels = 0 
