This image shows part of dynamical z-plane of discrete dynamical system  
 map fc is a complex quadratic polynomial : fc(z) = z^2 + c  
 black points show critical orbit = orbit of critical point z = 0   
   
iPeriodParent = 1 
iPeriodChild = 3 
InternalAngle or rotational number = 0.333333 
InternalAngle distorsion ( epsilon)  = 0.000080 
   
parameter of map fc :   
Cx  = -0.125435 
Cy  = 0.649519 
   
alfa fixed point  
alfax  = -0.250218 
alfay  = 0.432887 
   
corners of dynamical z-plane rectangle : 
  ZxMin = -0.750218 
 ZxMax = 0.249782 
 ZyMin = -0.192113 
 ZyMax = 0.807887 
iHeight  = 2000 
PixelWidth  = 0.000500 
Number of computed critical orbit points  = iterMax = 100000 
distorsion of image  = 1.000000 
Number of computed but clipped critical orbit points ( out of image so not drawn)  = iLostPixels = 0 
