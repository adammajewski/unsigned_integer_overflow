This image shows part of dynamical z-plane of discrete dynamical system  
 map fc is a complex quadratic polynomial : fc(z) = z^2 + c  
 black points show critical orbit = orbit of critical point z = 0   
   
Initial InternalAngle or rotational number = 1/3 + 0.110000 = 0.443333 
   
parameter of map fc :   
Cx  = -0.013750 
Cy  = 0.565082 
is located on iPeriodParent = 1 
iPeriodChild = 3 
   
alfa fixed point  
alfax  = -0.225000 
alfay  = 0.389711 
   
corners of dynamical z-plane rectangle : 
  ZxMin = -0.725000 
 ZxMax = 0.275000 
 ZyMin = -0.235289 
 ZyMax = 0.764711 
iHeight  = 2000 
PixelWidth  = 0.000500 
Number of computed critical orbit points  = iterMax = 10000000 
distorsion of image  = 1.000000 
Number of computed but clipped critical orbit points ( out of image so not drawn)  = iLostPixels = 0 
