/*

 
  c console progam   
 gcc t.c  -Wall -pedantic -lm
 gcc t.c -std=c99  -Wall -pedantic -lm


  gcc t.c  -Wall
  gcc -std=c11 -Wall -pedantic t.c
  gcc -std=c99 -Wall -pedantic t.c
  ./a.out


unsigned long long iterMax  =100000000000; 
       ULLONG_MAX =  18446744073709551615 
 INT_MAX = 2147483647 


*/



#include <stdio.h>
#include <math.h> // snprintf(name1,length+1,"%f", log10(iterMax)+k*step);
#include <float.h>

#define length 50

/* -----------------------------------------  main   -------------------------------------------------------------*/
int main()
{
  char name1 [length]; 
  char name2 [length];
  double step= 0.00001;
  unsigned long long int iterMax  =100000000000; 
  int k;
 
  
  
  for (k = 0; k <= 20; k++)  
    { printf("numbers :  k = %2d ; k*step = %f ;", k, k*step); 
      snprintf(name1,length+1,"%Lf", (long double) iterMax+k*step); /* length + 1 for the terminating null character. Although it is not printed, it still takes space in the buffer.  */
      snprintf(name2,length+1, " %f", k*step); /*  */
      printf("strings : k*step =  %s ; iterMax+k*step = %s \n",name2, name1);  

    }

printf ("LDBL_MAX = %Lf \n",LDBL_MAX);
 
  return 0;
}
