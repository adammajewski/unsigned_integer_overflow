This image shows rectangle part of dynamic plane of discrete complex dynamical system  z(n+1) = fc(zn) 
 where  fc(z)= z^2 + c 
with numerical approximation of parabolic Julia set 

parameter c is a root point between iPeriodParent = 1 and iPeriodOfChild  = 2 hyperbolic components of Mandelbrot set 
on the end of the internal ray of parent component of Mandelbrot set with angle = 1/2 in turns 
 c = ( -0.750000 ; 0.000000 ) 
 comment : Fatou components, Julia set, immediate basin of parabolic fixed point alfa 

parabolic alfa fixed point z = ( -0.500000 ; 0.000000 )  
 radius around parabolic fixed point AR =  0.100050 ; Pixel width = 0.002001   
 EscapeRadius ER =  2.000000 ; 
 Maxima number of iterations : iMax = 20000 

 dynamic plane : 
 from ZxMin = -2.000000 to ZxMax = 2.000000
 from ZyMin = -1.000000 to ZyMax = 1.000000

 No lost pixels : t = 0 ; iMax and AR is good 
 computations made with double type numbers 

made with c console program  
Adam Majewski  
