/* 
isnormal  example 

ISO C99
http://www.cplusplus.com/reference/cmath/isnormal/
http://www.gnu.org/software/libc/manual/html_node/Floating-Point-Classes.html
http://docs.oracle.com/cd/E19957-01/806-3568/ncg_math.html



compile with: 
gcc -std=c99 s.c -lm


run :
./a.out

*/


#include <stdio.h> /* printf */
#include <math.h> /* isnormal */
#include <string.h>
#include <errno.h>







int TestNumber(double x)
{
  int f; // flag

  f= isnormal(x);
  if (f) 
    printf (" = %g ; number is normal \n",x);
    else printf (" = %g ; number is not normal = denormal = subnormal \n",x);

  return f;

}



//----------------------------

int main()
{

  double d ;
  double MinNormal; 
  int flag;
  
  printf("begining  : %s\n", strerror(errno));
  d = 1.0 ; // normal
  flag = TestNumber(d);
  do 
  { 
   MinNormal=d;
   d /=2.0; // look for subnormal 
   flag = TestNumber(d);
   
   }
  while (flag);

  printf ("\n\nResults  : \n");
  printf ("number %f = %g = %e is a approximation of minimal positive double normal \n",MinNormal, MinNormal, MinNormal);
  printf ("number %f = %g = %e is not normal ( subnormal) \n",d, d , d);
  printf("errorno at the end = %d  : %s\n",errno,  strerror(errno));
  
  
  
  return 0;
}
