#include <stdio.h>
#include <math.h>
#include <complex.h> 
#include <gmp.h>
#include <mpfr.h>



double width2; // = width/2.0
double height2; // = height/2.0

/* 

what precision of floating point numbers do I need 
to draw/compute part of complex plane ( 2D rectangle ) ?
http://fraktal.republika.pl/mandel_zoom.html
https://en.wikibooks.org/wiki/Fractals/Computer_graphic_techniques/2D/plane
https://en.wikibooks.org/wiki/Fractals/Mathematics/Numerical

----------------------------------------------
http://www.hpdz.net/TechInfo/Bignum.htm

W > (d N) / (2 E)
2*W*E/N > d
d< 2*W*E/N 

W = size of image in the complex plane (determined by whichever direction is larger, usually the X-direction)
N = number of pixels in the larger direction (again, usually the X-direction)
E = the maximum proportional error we are willing to tolerate in pixel location (e.g. maybe 1% or 0.1%= 0.01 or something like that).
s = W / N
d = the smallest number that can be represented by one least-significant bit in the number system in us

 I usually switch to the next higher level of precision when the increment s becomes less than 1000 times the d-value for the current precision,
-------------------------------------------------------------------------------------------
http://mathr.co.uk/misc/2014-09-29_mandelbrot_render_mpfr.png

uses 71bit mpfr for exterior and 142bit mpfr for interior
I had very ugly bugs with 71bit for interior

coordinates:
  -2.273315146119407013919720794837553597868e-01 + 
1.115883747489345094864696755962163866559e+00 i 2.775557561562892e-17

code coming soon

took about 30mins (with 4-core CPU clamped to 800MHz, was overheating)

in comparison it's a few seconds with mightymandel perturbation on GPU 
(though mightymandel is simpler, no interior distance or period colour)







------------------------------
uses the code from 
https://gitorious.org/~claude
by  Claude Heiland-Allen

view = " cre cim radius"
view="-0.75 0 1.5" // standard view 
view="0.275336142511115 6.75982538465039e-3 0.666e-5" 
view="-0.16323108442468427133 1.03436384057374316916 1e-5" 
view= " -2.273315146119407013919720794837553597868e-01 +1.115883747489345094864696755962163866559e+00 i 2.775557561562892e-17"


gcc p.c -lmpfr -lgmp  -lm -Wall
*/


/* 
from /lib/mandelbrot_render_native.c :

complex FTYPE FNAME(coordinate)(int i, int j, int width, int height, complex FTYPE center, FTYPE radius) {
  FTYPE x = (i - width /FNAME(2.0)) / (height/FNAME(2.0));
  FTYPE y = (j - height/FNAME(2.0)) / (height/FNAME(2.0));
  complex FTYPE c = center + radius * (x - I * y);
  return c;
}

small changes 
*/

double complex coordinate(int i, int j, int width, int height, double complex center, double radius) 
{
  double x = (i - width2) / height2;
  double y = (j - height2) / height2;
  double complex c = center + radius * (x - I * y);
  return c;
}


int PrintComplex(double complex c, char *comment)
{
  printf("%s ( %.16e ; %.16e )\n ", comment, creal(c), cimag(c));
  return 0;
}


int main() 
{
  
  //declare variables 
  //integer coordinate ( pixels) of image 
  int height = 720;
  int width = 1280;
  double complex center= -0.75*I;
  double complex cRightUp, cLeftDown;
  double dWidth;
  double dWidth0 = 5.333333; // standard view : when radius = 1.5 and width =1280
  double zoom ; // magnification , see : https://en.wikibooks.org/wiki/Fractals/Computer_graphic_techniques/2D/plane#Description
  double dRadius = 2.775557561562892e-17;
  //double complex c;
  //int i,j;
  
  


  int float_type ;
  int pixel_spacing_bits;

  mpfr_t radius; // it's the radius (distance from center to side)
  mpfr_init2(radius, 53); //
  mpfr_set_d(radius, dRadius, GMP_RNDN); // ? why not MPFR_RNDD
  
  mpfr_t pixel_spacing;
  mpfr_init2(pixel_spacing, 53); //

  mpfr_t pixel_spacing_log; // pixel_spacing_log +8 = MPFR precision = number of binary digits
  mpfr_init2(pixel_spacing_log, 53);
  
  width2 = width /2.0;
  height2 = height/2.0;

  printf ("radius = "); mpfr_out_str (stdout, 10, 0, radius, MPFR_RNDD); putchar ('\n');

  // compute 
  // int mpfr_div_d (mpfr_t rop, mpfr_t op1, double op2, mpfr_rnd_t rnd)
  mpfr_div_d(pixel_spacing, radius, height / 2.0, GMP_RNDN); // pixel_spacing = radius / ( height / 2.0)
  printf ("pixel_spacing = "); mpfr_out_str (stdout, 10, 0, pixel_spacing, MPFR_RNDD); putchar ('\n');
  
  mpfr_log2(pixel_spacing_log, pixel_spacing, MPFR_RNDN); // pixel_spacing_log = log2( pixel_spacing )
  printf ("pixel_spacing_log = "); mpfr_out_str (stdout, 10, 0, pixel_spacing_log, MPFR_RNDD); putchar ('\n');

  pixel_spacing_bits = -mpfr_get_d(pixel_spacing_log, GMP_RNDN);
  printf ("pixel_spacing_bits = %d \n", pixel_spacing_bits);
  
  printf("\n------------------------------\n");
  PrintComplex(center,"center = "); 
  printf("Image resolution in pixels : iWidth = %d , height = %d\n", width, height);
  cRightUp = coordinate(width,height,width,height, center, mpfr_get_d (radius, MPFR_RNDD));
  PrintComplex(cRightUp, " right up corner = ");
  cLeftDown = coordinate(0,0,width,height, center, mpfr_get_d (radius, MPFR_RNDD));
  PrintComplex(cLeftDown, " left down corner = ");
  dWidth = creal(cRightUp)-creal(cLeftDown);
  printf("Image width in world units : dWidth = %.16e \n", dWidth);
  zoom = dWidth0/dWidth;
  printf("Image zoom = %.16e \n", zoom);


  float_type = 1; /* why not 0 = float ? " ... I benchmarked and float is slower than double here, 
so I skip float entirely..." Claudius */
  if (pixel_spacing_bits > 20) float_type = 1;
  if (pixel_spacing_bits > 50) float_type = 2;
  if (pixel_spacing_bits > 60) float_type = 3;
  



switch (float_type) {
      case 0: fprintf(stderr, "render using float \n"); break;
      case 1: fprintf(stderr, "render using double ( faster then float) \n"); break;
      case 2: fprintf(stderr, "render using long double \n"); break;
      case 3: fprintf(stderr, "render using MPFR with precision = %d binary digits  \n", pixel_spacing_bits+8); // " I add 8 to the pixel_spacing_log as a safety margin." 
        
    }




// test
for ( j = 0; j < height; ++j) {
    

return 0;

}
