#include <stdio.h>
#include <gmp.h>
#include <mpfr.h>

/* 

what precision of floating point numbers do I need 
to draw/compute part of complex plane ( 2D rectangle ) ?
http://fraktal.republika.pl/mandel_zoom.html
https://en.wikibooks.org/wiki/Fractals/Computer_graphic_techniques/2D/plane
https://en.wikibooks.org/wiki/Fractals/Mathematics/Numerical



uses the code from 
https://gitorious.org/~claude
by  Claude Heiland-Allen

view = " cre cim radius"
view="-0.75 0 1.5"
view="0.275336142511115 6.75982538465039e-3 0.666e-5" 
view="-0.16323108442468427133 1.03436384057374316916 1e-5" 


gcc p.c -lmpfr -lgmp  -Wall
*/

int main() 
{
  
  //declare variables 
  int height = 720;
  int float_type ;
  int pixel_spacing_bits;

  mpfr_t radius;
  mpfr_init2(radius, 53); //
  mpfr_set_d(radius, 1.0e-15, GMP_RNDN);
  
  mpfr_t pixel_spacing;
  mpfr_init2(pixel_spacing, 53); //

  mpfr_t pixel_spacing_log;
  mpfr_init2(pixel_spacing_log, 53);
  


  printf ("radius = "); mpfr_out_str (stdout, 10, 0, radius, MPFR_RNDD); putchar ('\n');

  // compute 
  // int mpfr_div_d (mpfr_t rop, mpfr_t op1, double op2, mpfr_rnd_t rnd)
  mpfr_div_d(pixel_spacing, radius, height / 2.0, GMP_RNDN);
  printf ("pixel_spacing = "); mpfr_out_str (stdout, 10, 0, pixel_spacing, MPFR_RNDD); putchar ('\n');
  
  mpfr_log2(pixel_spacing_log, pixel_spacing, MPFR_RNDN);
  printf ("pixel_spacing_log = "); mpfr_out_str (stdout, 10, 0, pixel_spacing_log, MPFR_RNDD); putchar ('\n');

  pixel_spacing_bits = -mpfr_get_d(pixel_spacing_log, GMP_RNDN);
  printf ("pixel_spacing_bits = %d \n", pixel_spacing_bits); 


  float_type = 0;
  if (pixel_spacing_bits > 40) float_type = 1;
  if (pixel_spacing_bits > 50) float_type = 2;
  if (pixel_spacing_bits > 60) float_type = 3;
  



switch (float_type) {
      case 0: fprintf(stderr, "render using float \n"); break;
      case 1: fprintf(stderr, "render using double \n"); break;
      case 2: fprintf(stderr, "render using long double \n"); break;
      case 3: fprintf(stderr, "render using MPFR - arbitrary precision \n");
        
    }
return 0;

}
