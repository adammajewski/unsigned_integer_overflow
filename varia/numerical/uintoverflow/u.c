#include <stdio.h>

/* 

c console program : example of Unsigned integer overflow 

Adam Majewski


 
 Unsigned integers are defined to wrap around.
 "When you work with unsigned types, modular arithmetic (also known as "wrap around" behavior) is taking place."
http://stackoverflow.com/questions/7221409/is-unsigned-integer-subtraction-defined-behavior

*/


int main() {

unsigned int i;
unsigned int old=0; // 
unsigned int new=0; // 
unsigned int p=1000000000; // 
//
unsigned long long int lnew= 0; //
unsigned long long int lold = (unsigned long long int) old; //
unsigned long long int lp = (unsigned long long int) p; //

printf("unsigned long long int \tunsigned  int \n"); // header 

for ( i=0 ; i<20; i++){
     printf("lnew = %12llu \tnew =  %12u",  lnew, new);
     // check overflow
     // http://stackoverflow.com/questions/2633661/how-to-check-integer-overflow-in-c/
     if ( new < old) printf("    unsigned integer overflow = wrap \n");
                     else printf("\n");          
                       
                        
     // unsigned int
     old=new; // save old value for comparison = overflow check 
     new = old + p ; // simple addition ; new value should be greater then old value 
     // unsigned long long int
     lold=lnew;
     lnew=lold+lp;      
}


return 0; 
}
