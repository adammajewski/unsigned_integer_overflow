// gcc p.c -lgmp
 
#include <stdio.h>
#include <gmp.h>
 
int main (int argc, char **argv)
{       
       


         mpf_set_default_prec(100024); // set default precision in bits


        // declare   
        mpz_t zn;
        mpz_t zd;
        mpq_t q;
        mpf_t fn;
        mpf_t fd; 
        mpf_t f; // f=float(q=n/d)
        
        // init
        mpz_init (zn);
        mpz_init (zd);
        mpq_init (q);
        mpf_init (fn) ; //794564201485273000257607338237654476912493997529945960250807965815440 ,100); 
        mpf_init (fd); 
        mpf_init (f); 
 
        // set
        mpz_set_str(zn, "33877456965431938318210482471113262183356704085033125021829876006886584214655562", 10 ); // 
        // mpz_set_ui(n,33877456965431938318210482471113262183356704085033125021829876006886584214655562);   warning: integer constant is too large for its type [enabled by default]
        mpz_set_str(zd, "237142198758023568227473377297792835283496928595231875152809132048206089502588927", 10);
        //mpq_set_str(q, "33877456965431938318210482471113262183356704085033125021829876006886584214655562/237142198758023568227473377297792835283496928595231875152809132048206089502588927", 10);

        mpf_set_z(fn, zn);
        mpf_set_z(fd, zd);
        


        //
        mpf_div(f, fn, fd);
        
        // print result on the screen
        printf (" ratio =  "); mpz_out_str(stdout, 10,zn);  printf (" / ");  mpz_out_str(stdout, 10,zd);  printf (" \n "); 
        gmp_printf ("  decimal floating point number : %.Ff \n", f); //


        FILE* fp;
        fp = fopen("num.txt", "w");
        mpf_out_str(fp, 10, 22222222222, fp);
        fclose(fp);
             
        // clear  
        mpz_clear (zn);
        mpz_clear (zd);
        mpq_clear(q);
        mpf_clear (fn);
        mpf_clear (fd);
        mpf_clear (f);
 
        return 0;
}
