// gcc f.c -lgmp

#include <stdlib.h>
#include <stdio.h>
#include "gmp.h"



void usage(const char *progname) {
  fprintf(stderr,
    "usage: %s  number \n"
    "example : \n %s 12\n", progname, progname);
}

 
int main (int argc, char *argv[]) {
 

     // read and check input
  if (argc < 2) { usage(argv[0]); return 1; }
     
    //
   mpf_t sq_me, sq_out, test;
 
    mpf_set_default_prec (1000);
 
    mpf_inits(sq_me, sq_out, test, NULL); /* use default precision */
 
    mpf_set_str (sq_me, argv[1], 10); // read ffrom input 
 
    mpf_sqrt(sq_out, sq_me);
    mpf_mul(test,sq_out,sq_out);
 
    gmp_printf ("Input:       %Ff\n\n", sq_me);
    gmp_printf ("Square root: %.200Ff\n\n", sq_out);
    gmp_printf ("Re-squared:  %Ff\n\n", test);
 
 
   mpf_clears(sq_me, sq_out, test, NULL);
 
    return 0;
}

